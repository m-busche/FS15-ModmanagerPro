@echo off
rem ********************************************************************
rem * Build script for FS15-ManagerPro                                 *
rem * Please create a local copy to match your path settings!          *
rem ********************************************************************
rem
rem ********************************************************************
rem * Important: Version has to be updated in                          *
rem * - build-release.cmd                                              *
rem * - Project properties - Assembly information                      *
rem * - setup.iss                                                      *
rem ********************************************************************
rem
rem ********************************************************************
rem * Start with 'beta[NUM]' or 'release' as commandline argument      *
rem * Add 'upload' as 2nd commandline argument for upload to webserver *
rem ********************************************************************
rem
if "%1"=="" (
	echo "Start with 'beta[NUM]' or 'release' as commandline argument"
	GOTO bye
	) else (
	GOTO build)

:build
rem Add path to MSBuild Binaries
IF EXIST "%ProgramFiles%\MSBuild\14.0\bin" SET PATH=%ProgramFiles%\MSBuild\14.0\bin;%PATH%
IF EXIST "%ProgramFiles(x86)%\MSBuild\14.0\bin" SET PATH=%ProgramFiles(x86)%\MSBuild\14.0\bin;%PATH%
SETLOCAL EnableDelayedExpansion

SET VERSION="1.0.0.9-%1"
SET MSBUILD=msbuild.exe
SET VS-SLN="FS15-ModmanagerPro.sln"
SET IS-COMPILE="D:\Program Files (x86)\Inno Setup 5\ISCC.exe"
SET ISCC_PARMS=/F"FS15-ModmanagerPro-%VERSION%"
SET IS-SRC=".\setup\setup.iss"
SET MSIWRAPPER="C:\Program Files (x86)\MSI Wrapper\MsiWrapperBatch.exe"
SET MSIWRAPPER_XML=".\setup\msiwrapper"
SET SEVENZIP="D:\Program Files\7-Zip\7z.exe"
SET PSCP="D:\Program Files (x86)\PuTTY\pscp.exe"
SET PRIVATEKEY="C:\Users\Markus\Documents\ssh-keys\uberspace-elpatron.ppk"

rem Create timestamp
rem Cut off fractional seconds
set t=%time:~0,8%
rem Replace colons with dashes
rem set t=%t::=-%
set TSTAMP=%date%-%t%

rem Replace version in Form1.vb
if %1==beta (
	call replacer.cmd .\FS15-ModmanagerPro\Form1.vb "Debugversion, not for distribution" "%VERSION% (%TSTAMP%)"
	) else (
	call replacer.cmd .\FS15-ModmanagerPro\Form1.vb "Debugversion, not for distribution" "%VERSION%")

rem Build x86 binaries
msbuild %VS-SLN% /t:build /p:Configuration=Release;Platform=x86
rem Build x64 binaries
msbuild %VS-SLN% /t:build /p:Configuration=Release;Platform=x64

rem Set version information back to Debugversion
call replacer.cmd .\FS15-ModmanagerPro\Form1.vb "%VERSION%" "Debugversion, not for distribution"

rem Build setup with Inno Setup
%IS-COMPILE% %ISCC_PARMS% "%IS-SRC%"

rem Replace version string in msiwrapper.xml
copy %MSIWRAPPER_XML%.tpl %MSIWRAPPER_XML%.xml
call replacer.cmd %MSIWRAPPER_XML%.xml "zccHd5fsa066" "%VERSION%"
rem Build MSI
%MSIWRAPPER% config=%MSIWRAPPER_XML%.xml
del %MSIWRAPPER_XML%.xml

rem Package Setup with 7Zip
%SEVENZIP% a .\setup\Output\FS15-ModmanagerPro-%VERSION%.zip .\setup\Output\FS15-ModmanagerPro-%VERSION%.exe CHANGELOG*.* README*.* LICENSE*.*

rem 32 bit portable distribution
cd /d .\FS15-ModmanagerPro\bin\x86\Release\
%SEVENZIP% a ..\..\..\..\setup\Output\FS15-ModmanagerPro-%VERSION%-portable-x32.zip *.exe *.dll de es

rem 64 bit portable distribution
cd /d .\FS15-ModmanagerPro\bin\x64\Release\
%SEVENZIP% a ..\..\..\..\setup\Output\FS15-ModmanagerPro-%VERSION%-portable-x64.zip *.exe *.dll de es

rem add license, changelog, readme
cd ..\..\..\..\
%SEVENZIP% a .\setup\Output\FS15-ModmanagerPro-%VERSION%-portable-x32.zip CHANGELOG*.* README*.* LICENSE*.*
%SEVENZIP% a .\setup\Output\FS15-ModmanagerPro-%VERSION%-portable-x64.zip CHANGELOG*.* README*.* LICENSE*.*

rem delete *.vshost.exe from portable zips
%SEVENZIP% d setup\Output\FS15-ModmanagerPro-%VERSION%-portable-x32.zip *.vshost.exe -r
%SEVENZIP% d setup\Output\FS15-ModmanagerPro-%VERSION%-portable-x64.zip *.vshost.exe -r

IF NOT "%2"=="upload" GOTO end

rem Create copy of dl.php
copy .\web\uberspace\dl.php .\web\uberspace\index.php 
rem Replace verion and timestamp in index.php
call replacer.cmd ".\web\uberspace\index.php" "zccHd5fsa066" "%VERSION%"
call replacer.cmd ".\web\uberspace\index.php" "mL0buw8z3HF7" "%TSTAMP%""

rem Upload files
echo Uploading FS15-ModmanagerPro-%VERSION%.exe to FS15-ModmanagerPro-setup-latest-%1%.exe...
%PSCP% -i "%PRIVATEKEY%" .\setup\Output\FS15-ModmanagerPro-%VERSION%.exe elpatron@cepheus.uberspace.de:/home/elpatron/html/fs15-modmanagerpro/files/FS15-ModmanagerPro-setup-latest-%1%.exe
pause
echo Uploading FS15-ModmanagerPro-%VERSION%.zip to FS15-ModmanagerPro-setup-latest-%1%.zip...
%PSCP% -i "%PRIVATEKEY%" .\setup\Output\FS15-ModmanagerPro-%VERSION%.zip elpatron@cepheus.uberspace.de:/home/elpatron/html/fs15-modmanagerpro/files/FS15-ModmanagerPro-setup-latest-%1%.zip
echo Uploading FS15-ModmanagerPro-%VERSION%.msi to FS15-ModmanagerPro-latest-%1%.msi...
%PSCP% -i "%PRIVATEKEY%" .\setup\Output\FS15-ModmanagerPro-%VERSION%.msi elpatron@cepheus.uberspace.de:/home/elpatron/html/fs15-modmanagerpro/files/FS15-ModmanagerPro-latest-%1%.msi
echo Uploading FS15-ModmanagerPro-%VERSION%-portable-x32.zip to FS15-ModmanagerPro-portable-x32-latest-%1%.zip...
%PSCP% -i "%PRIVATEKEY%" .\setup\Output\FS15-ModmanagerPro-%VERSION%-portable-x32.zip elpatron@cepheus.uberspace.de:/home/elpatron/html/fs15-modmanagerpro/files/FS15-ModmanagerPro-portable-x32-latest-%1%.zip
echo Uploading FS15-ModmanagerPro-%VERSION%-portable-x64.zip to FS15-ModmanagerPro-portable-x64-latest-%1%.zip...
%PSCP% -i "%PRIVATEKEY%" .\setup\Output\FS15-ModmanagerPro-%VERSION%-portable-x64.zip elpatron@cepheus.uberspace.de:/home/elpatron/html/fs15-modmanagerpro/files/FS15-ModmanagerPro-portable-x64-latest-%1%.zip
echo Uploading new index.php
%PSCP% -i "%PRIVATEKEY%" .\web\uberspace\index.php elpatron@cepheus.uberspace.de:/home/elpatron/html/fs15-modmanagerpro/index.php
echo Uploading CHANGELOG to CHANGELOG.txt
%PSCP% -i "%PRIVATEKEY%" CHANGELOG elpatron@cepheus.uberspace.de:/home/elpatron/html/fs15-modmanagerpro/files/CHANGELOG.txt
del .\web\uberspace\index.php 

rem Open output in explorer
:end
start "" explorer.exe ".\setup\Output\"

:bye
echo done.