# FS15-ModmanagerPro

This is not the first tool for managing Farming Simulator Mods. And probably
not the last. But maybe this tool is a little bit smarter than others.

![FS-15 ModManagerPro Screenshot](screenshots/main.png)

## What is it good for?

FS15-ModmanagerPro lets you organize your mod files in profiles. In each
profile you can activate/deactivate each single mod. There are other tools 
which do the same. We just do it in a smarter way.

## Why should you use FS15-ModmanagerPro?

Your mod files are being saved just once on your harddrive - independently of 
the number of profiles. Other mod management tools copy each mod into a 
directory for each profile. FS15-ModmanagerPro uses a feature of modern 
filesystems like NTFS called symbolic links. Each (mod)file is strored only 
once on your harddisk and is linked to your profile directories. Thus, a lot of 
disk space can be saved. And you gain full control over your mods and versions 
of mods files for all your profiles.
  * There is no limit in the number of profiles and mods.
  * You only have to take care of one single mod archive.
  * FS15-ModmanagerPro is ultra fast. Creating hundrets of symbolic links lasts only a few milliseconds!
  * If you downloaded an update for a mod: Just replace the file in your mod repository. That´s it.
  * Clear overview over your mods and profiles.
  * Mod settings in the form of XML files are symlinked to your profiles. Just create them once!
  * Detailed information about any mod including images.
  * FS15-ModmanagerPro is OpenSource Software, you can freely distribute or change it.
  * FS15-ModmanagerPro is designed multilingual (English, Spanish and German implemented)

![FS-15 ModManagerPro Mod Info Screenshot](screenshots/FarmingSimulator2015_modmanager-pro_modinfo.png)

## Requirements

Of course, a version of Giants Farming Simulator 2015 is required. Furthermore, 
Microsoft .NET Framework 4.5.1 and a NTFS formatted partition is needed.

## Installation

This download includes a 'setup.exe' file, which can be installed the usual way.
The program leads you through the first steps in creating your own mod archive
and profiles.

## Restriction

FS15-ModmanagerPro needs elevated rights to create the symbolic links in your 
filesystem. This is due to a restriction from Microsoft Windows.

## Known Issues

The setup installer in some cases seems to have problems detecting if vcredis is installed yet. Canceling
vcredis installer and proceeding seems to be no problem.

## Homepage

Sourcecode, Issues, feature wishes at [Gitlab](https://gitlab.com/m-busche/FS15-ModmanagerPro).