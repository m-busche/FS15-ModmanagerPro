v1.0.0.9 (15.5.2016)
  * Added Russian langauge (by Vasiliy)
  * Remind player to activate / plug in game controller (see File - Settings)

v1.0.0.7
  * Añadida función de búsqueda (Crtl-F)
  * Rearranged menu structure
  * Cambiado atajo teclado para iniciar FS15 a "F5"
  * Solucionado error con archivos zip rotos (Issue #44)
  * Mostar los mods totales/activatados por perfil (Issue #47)
  * Chequear si el juego esta en marcha antes de salvar cambios o eliminar un mod
  * Solucionado problema con uso de caracteres no permitidos en nombre de perfil (path)
  * Fixed a problem with partly not displaying german translation strings

v1.0.0.6
  * Crear Symlinks a carpetas de mods descomprimidos
  * Borrar completamente un mod
  * Activar un mod en todos los perfiles
  * Duplicar/Clonar perfil
  * Nuevo atajo de teclado F2: Renombrar perfil
  * Nuevo atajo de teclado Crtl-R: Volver a cargar mods y perfiles 

v1.0.0.5 
  * Traducción al Aleman y Español del CHANGELOG, README y LICENSE
  * Solucionado error crítico (Issue #36): Fallo en el programa al inicio si no existiera algún archivo de los symlinks almacenados.

v1.0.0.4
  * Estructura en columnas (perfiles) y filas (mods)
  * Traducción al español (gracias, PromGames)
  * Control de funciones a traves de combinaciones de teclado
  * Información del Mod, mostrando su imagen (via Crtl-I o usando el menú)
  * (Seleccionable) También se pueden unir los archivos de configuration de mods (GPS.xml, Glance.xml)
  * Distintos colores para identificar los tipos de mods (script/rojo) (mapa/verde) (resto/blanco)
  * Algunas mejoras y solución de errores