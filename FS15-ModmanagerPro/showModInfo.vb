﻿'Imports System.IO
'Imports ImageMagick

Public Class showModInfo
    Public ilImageList As ImageList
    Dim iImageIndex As Integer = 0
    Dim ImageCount As Integer = 0

    Private Sub showModInfo_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.KeyPreview = True
        ImageCount = ilImageList.Images.Count
        If Not ilImageList.Images.Empty Then
            PictureBox1.Image = ilImageList.Images(0)
            PictureBox1.SizeMode = PictureBoxSizeMode.Zoom
        End If

        If ilImageList.Images.Count = 0 Then
            Label1.Text = "No image available"
        Else
            Label1.Text = String.Format("Image {0}/{1}", iImageIndex + 1.ToString, ImageCount.ToString)
        End If
    End Sub

    Private Sub btnClose_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        Me.Dispose()
    End Sub

    Private Sub showModInfo_KeyDown(sender As Object, e As KeyEventArgs) Handles MyBase.KeyDown
        If e.KeyCode = Keys.Escape Then
            Me.Dispose()
        End If
    End Sub

    Private Sub PictureBox1_Click(sender As Object, e As EventArgs) Handles PictureBox1.Click
        If ImageCount > 1 Then
            If iImageIndex < ImageCount - 1 Then
                iImageIndex += 1
            Else
                iImageIndex = 0
            End If
            PictureBox1.Image = ilImageList.Images(iImageIndex)
            Label1.Text = String.Format("Image {0}/{1}", iImageIndex + 1.ToString, ImageCount.ToString)
        End If
    End Sub
End Class