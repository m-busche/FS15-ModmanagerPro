﻿' Open source code sign certificate certum.pl: https://cservices.certum.pl/muc-customer/customerCertificate/list
Imports System.IO
Imports System.Collections.Specialized
Imports System.Globalization
Imports System.Threading
Imports Microsoft.Win32
Imports System.ComponentModel

Public Class Form1
    Dim sModRepoFolder As String
    Dim sProfilesFolder As String
    Dim sGameSettingsXML As String
    Dim sGameExeFile As String
    Dim bGameControllerReminder As Boolean
    Public sSettingsFile As String
    Private WithEvents Dgv As New DataGridView
    Dim ci As CultureInfo = CultureInfo.CurrentCulture
    Dim Controlcolor As Color
    Dim MapColor As Color = Color.LightGreen
    Dim ModConfigColor As Color = Color.LightCyan
    Dim ModColor As Color = Color.LightYellow
    Dim ScriptColor As Color = Color.LightSalmon
    Dim DirectoryColor As Color = Color.Aqua
    Dim lastSearch As clLastSearch


    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        ' ChangeLanguage("es")
        KeyPreview = True
        ' Text = "FS15-ModmanagerPro " + "Debugversion, not for distribution"
        lastSearch = New clLastSearch
        btnSave.Text = btnSave.Text + vbCrLf + "[Crtl-S]"
        btnActivateProfile.Text = btnActivateProfile.Text + vbCrLf + ("[Crtl-P]")
        btnStartFS15.Text = btnStartFS15.Text + vbCrLf + "[F5]"
        ModInfoToolStripMenuItem.Text = ModInfoToolStripMenuItem.Text + " [Crtl-I]"
        NewRpfileToolStripMenuItem.Text = NewRpfileToolStripMenuItem.Text + " [Crtl-N]"
        sSettingsFile = Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData) + "\FS15-ModmanagerPro\settings.xml"
        Me.Text = Me.Text + " " + String.Format("Version {0}", My.Application.Info.Version.ToString)
        Controlcolor = btnSave.BackColor
        ' We have to be admin to use symbolic links
        If Not IsAdmin() Then
            If Not File.Exists(sSettingsFile) Then
                ' First start information about admin rights
                MsgBox(ResourceManager.GetString("isadmin", ci), MsgBoxStyle.Information)
            End If
            ' still not admin?
            RestartElevated()
            If Not IsAdmin() Then
                Me.Dispose()
            End If
        End If

        ' Restore Location and size
        ' This seems not to work because of elevated start
        If Not My.Settings.Form1Location.IsEmpty Then
            Location = My.Settings.Form1Location
        End If

        If Not My.Settings.Form1Size.IsEmpty Then
            Size = My.Settings.Form1Size
        End If

        If loadSettings() = False Then
            If File.Exists(sSettingsFile) Then
                File.Delete(sSettingsFile)
            End If
            MsgBox(ResourceManager.GetString("settingsproblem", ci), MsgBoxStyle.Critical)
            Me.Dispose()
        End If

        ' Check if filesystem is NTFS formatted
        If sModRepoFolder <> String.Empty Then
            If SymbolicLink.checkNTFS(CChar(sModRepoFolder.Substring(0, 1))) = False Then
                MsgBox(ResourceManager.GetString("nontfs", ci), MsgBoxStyle.Critical)
                File.Delete(sSettingsFile)
                Me.Dispose()
            End If
            ' Check for write permission
            If SymbolicLink.HasWritePermissionOnDir(sModRepoFolder) = False Then
                MsgBox(ResourceManager.GetString("nowritepermission", ci), MsgBoxStyle.Critical)
                Me.Dispose()
            End If

            createProfilesFolder(sProfilesFolder)
            fillDatagrid()
            displayActiveProfile()
        End If


        btnSave.BackColor = Controlcolor
        ' This just causes Dgv to refresh (obsolete)
        ' SendKeys.Send("{tab}")
    End Sub

    ''' <summary>
    ''' Displays active profile in statusbar
    ''' </summary>
    Private Sub displayActiveProfile()
        Dim activeprofile() As String = FS15ConfigFile.readActiveConfiguration(sGameSettingsXML)
        If activeprofile(0) = "-1" Then
            ' Lo siento, archivo no encontrado. Compruebe la configuración.
            ' Sorry, {0} not found! Please check your settings.
            MsgBox(ResourceManager.GetString("gamesettingsnotfound", ci), MsgBoxStyle.Critical)
        Else
            TSSLProfile.Text = ResourceManager.GetString("activeprofile", ci) + activeprofile(0)
            TSSLProfile.ToolTipText = ResourceManager.GetString("fullpath", ci) + activeprofile(1) + ")"
            StatusStrip1.ShowItemToolTips = True
            _CountActiveMods()
        End If
    End Sub

    ''' <summary>
    ''' Load and validate settings
    ''' </summary>
    ''' <returns></returns>
    Private Function loadSettings() As Boolean
        ' do we have a settings file yet?
        If File.Exists(sSettingsFile) Then
            Dim sSettings() As String = readsettings()
            sGameSettingsXML = sSettings(0)
            sModRepoFolder = sSettings(1)
            sGameExeFile = sSettings(2)
            Try
                If sSettings(3) = "True" Then
                    bGameControllerReminder = True
                Else
                    bGameControllerReminder = False
                End If
            Catch ex As Exception

            End Try

            sProfilesFolder = sModRepoFolder + "\profiles"
            If sGameSettingsXML IsNot Nothing And sModRepoFolder IsNot Nothing And sGameExeFile IsNot Nothing Then
                Return True
            Else
                Return False
            End If
        Else
            ' we don´t have a settings file so show settings dialog
            If welcome.ShowDialog() = DialogResult.Cancel Then
                Me.Dispose()
            End If

            ' system to detect all possible situations of user installation including steam
            'first we will look for myDocuments
            Using rk As RegistryKey = Registry.CurrentUser.OpenSubKey("Software\Microsoft\Windows\CurrentVersion\Explorer\Shell Folders")
                If rk IsNot Nothing Then
                    Dim DocsPath As [Object] = rk.GetValue("Personal")
                    If DocsPath IsNot Nothing Then
                        My.Settings.DocsPath = DocsPath
                        My.Settings.Save()
                        Settings.txtConfigFile.Text = Convert.ToString(My.Settings.DocsPath) +
                    "\my games\FarmingSimulator2015\gameSettings.xml"
                    End If
                    'MessageBox.Show(DocsPath.ToString)
                    'MessageBox.Show("No Documents Folder Configuration Found")
                End If
            End Using
            ' I will look now for the game folder in the user's computer

            Using st32k As RegistryKey = Registry.LocalMachine.OpenSubKey("SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\Steam App 313160")
                If st32k IsNot Nothing Then
                    Dim St32GamePath As String = st32k.GetValue("InstallLocation")
                    My.Settings.St32GamePath = St32GamePath
                    My.Settings.Save()
                    'MessageBox.Show(St32GamePath.ToString)
                End If
                'MessageBox.Show("No Steam Configuration 32 Bytes Found")
            End Using
            Using st64k As RegistryKey = Registry.LocalMachine.OpenSubKey("SOFTWARE\Wow6432Node\Microsoft\Windows\CurrentVersion\Uninstall\Steam App 313160")
                If st64k IsNot Nothing Then
                    Dim St64GamePath As String = st64k.GetValue("InstallLocation")
                    My.Settings.St64GamePath = St64GamePath
                    My.Settings.Save()
                    'MessageBox.Show(St64GamePath.ToString)
                End If
                'MessageBox.Show("No Steam Configuration 64 Bytes Found")
            End Using
            Using FS64k As RegistryKey = Registry.LocalMachine.OpenSubKey("SOFTWARE\Wow6432Node\Microsoft\Windows\CurrentVersion\Uninstall\FarmingSimulator2015INT_is1")
                If FS64k IsNot Nothing Then
                    Dim FS64GamePath As String = FS64k.GetValue("InstallLocation")
                    My.Settings.FS64kGamePath = FS64GamePath
                    My.Settings.Save()
                    'MessageBox.Show(FS64GamePath.ToString)
                End If
            End Using


            Dim FSGamePath = Path.Combine(My.Settings.FS64kGamePath, My.Settings.St32GamePath, My.Settings.St64GamePath)
            My.Settings.FSGamePath = FSGamePath
            My.Settings.Save()

            If File.Exists(My.Settings.FSGamePath + "\FarmingSimulator2015.exe") Then
                Settings.txtGameExecutable.Text = My.Settings.FSGamePath + "FarmingSimulator2015.exe"
            End If

            If Directory.Exists(My.Settings.DocsPath + "\my games\FarmingSimulator2015\mods") Then
                Settings.txtModRepo.Text = My.Settings.DocsPath + "\my games\FarmingSimulator2015\mods"
            End If

            If Settings.ShowDialog() = DialogResult.OK Then
                ' user accepted settings, let´s see if we´re okay?
                If Not File.Exists(sSettingsFile) Then
                    ' no gamesettings.xml!
                    MsgBox(sSettingsFile + ResourceManager.GetString("gamesettingsnotfound", ci), MsgBoxStyle.Information)
                    Return False
                    Exit Function
                End If

                ' read settings
                Dim sSettings() As String = readsettings()

                sGameSettingsXML = sSettings(0)
                sModRepoFolder = sSettings(1)
                sProfilesFolder = sModRepoFolder + "\profiles"
                sGameExeFile = sSettings(2)

                ' do we have all values
                If sGameSettingsXML Is Nothing Or sModRepoFolder Is Nothing Or sGameExeFile Is Nothing Then
                    ' something is missing
                    MsgBox(ResourceManager.GetString("failedloadingsettings", ci), MsgBoxStyle.Critical)
                    Return False
                End If

                createProfilesFolder(sProfilesFolder)

                ' try to create mod repository if not there yet
                If Not Directory.Exists(sModRepoFolder) Then
                    Try
                        Directory.CreateDirectory(sModRepoFolder)
                    Catch
                        MsgBox(ResourceManager.GetString("errordirectorycreation", ci) + sModRepoFolder +
                               ResourceManager.GetString("checksettings", ci))
                        Return False

                    End Try
                End If
            Else
                Return False
            End If
        End If
        Return True
    End Function

    ''' <summary>
    ''' Creates profile directory
    ''' </summary>
    ''' <param name="sProfilesFolder"></param>
    Private Sub createProfilesFolder(sProfilesFolder As String)
        ' try to create profiles folder if not there yet
        If Not Directory.Exists(sProfilesFolder) Then
            Try
                Directory.CreateDirectory(sProfilesFolder)
            Catch
                MsgBox(ResourceManager.GetString("errordirectorycreation", ci) + sProfilesFolder +
                           ResourceManager.GetString("checksettings", ci))
            End Try
        End If

    End Sub

    ''' <summary>
    ''' Fills Datagridview on startup and refresh
    ''' </summary>
    Private Sub fillDatagrid()
        If Directory.GetFiles(sModRepoFolder, "*.zip").Length = 0 Then
            MsgBox(ResourceManager.GetString("nozipfiles", ci) + sModRepoFolder +
                   ResourceManager.GetString("addmods", ci), MsgBoxStyle.Information)
            Exit Sub
        End If
        Dim sModFiles() As String = Directory.GetFiles(sModRepoFolder, "*.zip")
        TSSLTotalMods.Text = ResourceManager.GetString("TotalMods", ci) + sModFiles.Length.ToString
        Dim sModConfigFiles() As String = Directory.GetFiles(sModRepoFolder, "*.xml")
        For iCount As Integer = 0 To sModFiles.Length - 1
            sModFiles(iCount) = Path.GetFileNameWithoutExtension(sModFiles(iCount))
        Next

        ' Add an sortable test column
        'Dim TextColumn As DataGridViewTextBoxColumn = New DataGridViewTextBoxColumn
        'TextColumn.HeaderText = "Category"
        'TextColumn.Name = "Category"
        'TextColumn.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells
        'Dgv.Columns.Add(TextColumn)

        Dim profiles() As String = readprofiles(sProfilesFolder)
        Dim CheckBoxColumn As DataGridViewCheckBoxColumn
        If profiles.Length > 0 Then
            checkForOrphanedSymlinks(profiles)
            For iColumnIndex As Integer = 0 To profiles.Length - 1
                CheckBoxColumn = New DataGridViewCheckBoxColumn
                CheckBoxColumn.HeaderText = profiles(iColumnIndex)
                CheckBoxColumn.Name = profiles(iColumnIndex)
                CheckBoxColumn.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells
                Dgv.Columns.Add(CheckBoxColumn)
            Next
        Else
            ' Add an empty profile
            CheckBoxColumn = New DataGridViewCheckBoxColumn
            CheckBoxColumn.HeaderText = "Profile 1"
            CheckBoxColumn.Name = "Profile 1"
            CheckBoxColumn.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells
            Dgv.Columns.Add(CheckBoxColumn)
        End If

        ' Add rows with mods
        If sModFiles.Length > 0 Then
            Dim iRowIndex As Integer = 0
            Dim newRow(Dgv.ColumnCount) As String
            Dim sThisProfilePath As String = ""
            Dim sModConfigFileNameWithoutPath As String = ""
            Dim thisModType As modType = New modType
            For Each sMod As String In sModFiles
                thisModType = modInfo.checkMod(sModRepoFolder + "\" + sMod + ".zip")
                ' Ignore non-mod zip files
                If thisModType.isMod = True Then
                    For iColumn As Integer = 0 To profiles.Length - 1
                        sThisProfilePath = sProfilesFolder + "\" + Dgv.Columns(iColumn).HeaderText + "\"
                        sModConfigFileNameWithoutPath = sMod + ".zip"
                        If File.Exists(sThisProfilePath + sModConfigFileNameWithoutPath) Then
                            Debug.WriteLine("Set True")
                            newRow(iColumn) = "True"
                        Else
                            Debug.WriteLine("Set False")
                            newRow(iColumn) = "False"
                        End If
                    Next
                    With Dgv
                        .Rows.Add(newRow)
                        .Rows(iRowIndex).DefaultCellStyle.BackColor = ModColor
                        ' Set different color for maps
                        If thisModType.isMap = True Then
                            .Rows(iRowIndex).DefaultCellStyle.BackColor = MapColor
                        End If
                        ' Set different colors for Scripts
                        If thisModType.isScript = True And Not thisModType.isMap = True Then
                            .Rows(iRowIndex).DefaultCellStyle.BackColor = ScriptColor
                        End If
                        .Rows(iRowIndex).HeaderCell.Value = sMod
                        .RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders
                    End With
                    iRowIndex += 1
                End If
            Next
            ' add rows with mod config files (*.xml)
            For Each sModConfig As String In sModConfigFiles

                For iColumn As Integer = 0 To profiles.Length - 1
                    sThisProfilePath = sProfilesFolder + "\" + Dgv.Columns(iColumn).HeaderText + "\"
                    sModConfigFileNameWithoutPath = Path.GetFileName(sModConfig)
                    If File.Exists(sThisProfilePath + sModConfigFileNameWithoutPath) Then
                        Debug.WriteLine("Set True")
                        newRow(iColumn) = "True"
                    Else
                        Debug.WriteLine("Set False")
                        newRow(iColumn) = "False"
                    End If
                Next
                With Dgv
                    .Rows.Add(newRow)
                    .Rows(iRowIndex).DefaultCellStyle.BackColor = ModConfigColor
                    .Rows(iRowIndex).HeaderCell.Value = sModConfigFileNameWithoutPath
                    .RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders
                End With
                iRowIndex += 1
            Next

            ' add rows for mod directories
            Dim sModDirectoryWithoutPath As String = ""
            For Each sModDirectory As String In Directory.GetDirectories(sModRepoFolder)
                sModDirectoryWithoutPath = New DirectoryInfo(sModDirectory).Name
                ' we don´t want our profiles folder to be added here!
                If Not sModDirectory.Contains("profiles") Then
                    If File.Exists(sModDirectory + "\moddesc.xml") Then
                        For iColumn As Integer = 0 To profiles.Length - 1
                            sThisProfilePath = sProfilesFolder + "\" + Dgv.Columns(iColumn).HeaderText + "\"

                            If Directory.Exists(sThisProfilePath + sModDirectoryWithoutPath) Then
                                Debug.WriteLine("Set True")
                                newRow(iColumn) = "True"
                            Else
                                Debug.WriteLine("Set False")
                                newRow(iColumn) = "False"
                            End If
                        Next
                        With Dgv
                            .Rows.Add(newRow)
                            .Rows(iRowIndex).DefaultCellStyle.BackColor = DirectoryColor
                            .Rows(iRowIndex).HeaderCell.Value = sModDirectoryWithoutPath
                            .RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders
                        End With
                        iRowIndex += 1
                    End If
                End If
            Next
        Else
            MsgBox(ResourceManager.GetString("nomods", ci), MsgBoxStyle.Information)
        End If

        ' Fill first column
        'For iCount As Integer = 0 To sModFiles.Length - 1
        '    Dgv.Rows(iCount).Cells(0).Value = iCount.ToString
        'Next

        ' Set some properties
        With Dgv
            .Dock = DockStyle.Fill
            .AllowUserToAddRows = False
            '.SelectionMode = DataGridViewSelectionMode.ColumnHeaderSelect
            .MultiSelect = False
            .AllowUserToResizeRows = False
        End With

        Me.Panel1.Controls.Add(Dgv)
    End Sub

    ''' <summary>
    ''' checks for orphaned symlinks to non existing mod files
    ''' </summary>
    ''' <param name="profiles"></param>
    Private Sub checkForOrphanedSymlinks(profiles() As String)
        Dim sModFile As String = ""
        For Each sProfile As String In profiles
            sProfile = sProfilesFolder + "\" + sProfile
            For Each sFile As String In Directory.GetFiles(sProfile)
                sModFile = sModRepoFolder + "\" + Path.GetFileName(sFile)
                If Not File.Exists(sModFile) Then
                    Try
                        File.Delete(sFile)
                        Debug.WriteLine("Deleting orphaned symlink: " + sFile)
                    Catch ex As Exception
                        Debug.WriteLine(ex.Message)
                    End Try
                End If
            Next
        Next
    End Sub

    ''' <summary>
    ''' Save changes and create/refresh symlinks
    ''' </summary>
    Private Sub saveProfiles()
        If _IsFS15Active() = True Then
            Exit Sub
        End If
        Dim profiles As StringCollection = New StringCollection
        For iCount As Integer = 0 To Dgv.ColumnCount - 1
            profiles.Add(Dgv.Columns(iCount).HeaderText)
        Next
        Dim sWatch As Stopwatch = New Stopwatch
        sWatch.Start()
        createSymlinks()
        sWatch.Stop()
        displayActiveProfile()
        TSSLProfile.Text = TSSLProfile.Text + " [" + sWatch.ElapsedMilliseconds.ToString + " ms]"
        btnSave.BackColor = Controlcolor
    End Sub

    ''' <summary>
    ''' Activates the selected profile and writes gameSettings.xml
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub btnActivateProfile_Click(sender As Object, e As EventArgs) Handles btnActivateProfile.Click
        If File.Exists(sGameSettingsXML) Then
            Dim sProfile As String = FS15ConfigFile.replaceConfig(sGameSettingsXML, sProfilesFolder, Dgv)
            displayActiveProfile()
        Else
            MsgBox(String.Format("Sorry, {0} not found!", sGameSettingsXML), MsgBoxStyle.Critical)
        End If
    End Sub

    ''' <summary>
    ''' Creates/Deletes (=refreshes) all symlinks for all profiles
    ''' </summary>
    Private Sub createSymlinks()
        Dim sSourceModFile As String = ""
        Dim sSymlinkModFile As String = ""
        Dim sThisProfileFolder As String = ""
        'For Each row As DataGridViewRow In Dgv.Rows
        For iRow As Integer = 0 To Dgv.RowCount - 1
            Debug.WriteLine("Dgv rows: " + (Dgv.RowCount - 1).ToString)

            Dim row As DataGridViewRow = Dgv.Rows(iRow)
            Dim bIsDirectory As Boolean = False
            ' iterate through all columns
            For iColumn = 0 To Dgv.ColumnCount - 1
                Debug.WriteLine(vbCrLf)
                Debug.WriteLine("Row/Column: " + iRow.ToString + "/" + iColumn.ToString)

                sThisProfileFolder = sProfilesFolder + "\" + Dgv.Columns(iColumn).HeaderCell.Value
                Debug.WriteLine("Profilefolder: " + sThisProfileFolder)

                ' distinguish between mods (+ .zip), xml files and unpacked mod directories
                If Directory.Exists(sModRepoFolder + "\" + Dgv.Rows(iRow).HeaderCell.Value.ToString) Then
                    bIsDirectory = True
                Else
                    bIsDirectory = False
                End If
                If Not Dgv.Rows(iRow).HeaderCell.Value.ToString.ToLower.Contains(".xml") And Not bIsDirectory Then
                    sSymlinkModFile = sThisProfileFolder + "\" + Dgv.Rows(iRow).HeaderCell.Value + ".zip"
                    sSourceModFile = sModRepoFolder + "\" + Dgv.Rows(iRow).HeaderCell.Value + ".zip"
                Else
                    sSymlinkModFile = sThisProfileFolder + "\" + Dgv.Rows(iRow).HeaderCell.Value
                    sSourceModFile = sModRepoFolder + "\" + Dgv.Rows(iRow).HeaderCell.Value
                End If

                ' don´t try to copy empty rows!
                If Dgv.Rows(iRow).HeaderCell.Value <> "" And Dgv.Rows(iRow).HeaderCell.Value IsNot Nothing Then
                    If row.Cells(iColumn).Value = True Then
                        ' create symlinks for activated mods
                        Debug.WriteLine("Create symlink")
                        Debug.WriteLine("Symlinkmodfile: " + sSymlinkModFile)
                        Debug.WriteLine("Sourcemodfile: " + sSourceModFile)
                        ' create directory
                        If Not Directory.Exists(sThisProfileFolder) Then
                            Directory.CreateDirectory(sThisProfileFolder)
                        End If
                        ' create symlink

                        SymbolicLink.CreateSymbolicLink(sSourceModFile, sSymlinkModFile, True)
                    Else
                        Debug.WriteLine("Delete symlink")
                        Debug.WriteLine("Symlinkmodfile: " + sSymlinkModFile)
                        Debug.WriteLine("Sourcemodfile: " + sSourceModFile)
                        SymbolicLink.DeleteSymbolicLink(sSymlinkModFile)
                    End If
                End If
            Next
        Next
    End Sub

    ''' <summary>
    ''' Toggles all mods to True or False
    ''' </summary>
    ''' <param name="bActive"></param>
    Private Sub toggleAll(ByVal bActive As Boolean)
        ' Loop trough selected column (profile) and (de)activate all mods
        Dim iColumn As Integer = Dgv.SelectedCells(0).ColumnIndex
        For iCount As Integer = 0 To Dgv.Rows.Count - 1
            Dgv.Rows(iCount).Cells(iColumn).Value = bActive
        Next
        Dgv.Refresh()
        ' This just causes Dgv to refresh
        SendKeys.Send("{tab}")
    End Sub

    ''' <summary>
    ''' Changes UI language (for testing only)
    ''' </summary>
    ''' <param name="culture"></param>
    Public Sub SetCultureInfo(culture As String)
        ' culture could be 'en-US', 'de-DE' etc...
        Dim myCultureInfo As New CultureInfo(culture)
        Thread.CurrentThread.CurrentCulture = myCultureInfo
        Thread.CurrentThread.CurrentUICulture = myCultureInfo
        InitializeComponent()
    End Sub

    ''' <summary>
    ''' Opens selected profile in explorer.exe
    ''' </summary>
    Private Sub openProfileInExplorer()
        Dim iColumn As Integer = Dgv.SelectedCells(0).ColumnIndex
        Dim sCurrentProfilefolder As String = Dgv.Columns(iColumn).HeaderCell.Value
        Process.Start("explorer.exe", sProfilesFolder + "\" + sCurrentProfilefolder)
    End Sub

    ''' <summary>
    ''' Deletes the selcted profile
    ''' </summary>
    Private Sub deleteSelectedProfile()
        Dim iColumn As Integer = Dgv.SelectedCells(0).ColumnIndex
        If Dgv.Columns(iColumn).HeaderCell.Value IsNot Nothing Then
            Dim sCurrentProfilefolder As String = Dgv.Columns(iColumn).HeaderCell.Value
            sCurrentProfilefolder = sProfilesFolder + "\" + sCurrentProfilefolder
            If Directory.Exists(sCurrentProfilefolder) Then
                Dim answer As MsgBoxResult = MsgBox(ResourceManager.GetString("thedirectory", ci) + sCurrentProfilefolder +
                                                    ResourceManager.GetString("willbedeleted", ci), MsgBoxStyle.YesNo)
                If answer = MsgBoxResult.Yes Then
                    Directory.Delete(sCurrentProfilefolder, True)
                    Dgv.Columns.Remove(Dgv.Columns(iColumn).Name)
                End If
            Else
                MsgBox(ResourceManager.GetString("theprofiledirectory", ci) + sCurrentProfilefolder +
                       ResourceManager.GetString("dowsnotexist", ci))
            End If

        End If
    End Sub

    ''' <summary>
    ''' Deletes the selected mod file
    ''' </summary>
    Private Sub DeleteSelectedMod()
        If _IsFS15Active() = True Then
            Exit Sub
        End If

        Dim sModName As String = sModRepoFolder + "\" + Dgv.CurrentRow.HeaderCell.Value
        Dim bChanged As Boolean = False
        ' we have to distinguish between zipped mod file and unzipped directory
        If File.Exists(sModName + ".zip") Then
            Dim sThisModFile As String = sModName + ".zip"
            If MsgBox("Are you sure to delete the file " + sThisModFile + "? (This will save the pending changes to your profiles, too)", MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then
                Try
                    File.Delete(sThisModFile)
                    bChanged = True
                Catch ex As Exception
                    Debug.WriteLine(ex.Message)
                End Try
            End If
        ElseIf Directory.Exists(sModName) Then
            If MsgBox("Are you sure to delete the directory " + sModName + "? (This will save the pending changes to your profiles, too)", MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then
                Try
                    Directory.Delete(sModName, True)
                    bChanged = True
                Catch ex As Exception
                    Debug.WriteLine(ex.Message)
                End Try
            End If
        Else
            Exit Sub
        End If

        ' Save changes and refresh Datagrid
        If bChanged = True Then
            _refreshDatagrid(False)
            createSymlinks()
        End If
    End Sub

    ''' <summary>
    ''' Activates the selected mod in all profiles
    ''' </summary>
    Private Sub _activateModInAllProfiles()
        Dim iRowIndex As Integer = Dgv.SelectedCells(0).RowIndex
        Dim selectedRow As DataGridViewRow = Dgv.Rows(iRowIndex)
        For Each sProfileCell As DataGridViewCell In selectedRow.Cells
            sProfileCell.Value = "True"
        Next
        ' This just causes Dgv to refresh
        If iRowIndex < Dgv.Rows.Count - 1 Then
            SendKeys.Send("{down}")
            SendKeys.Send("{up}")
        Else
            SendKeys.Send("{up}")
            SendKeys.Send("{down}")
        End If
    End Sub

    ''' <summary>
    ''' Adds a new profile (bNew = True) or renames an existing profile (bNew = False)
    ''' </summary>
    ''' <param name="bNew"></param>
    Private Sub newOrRenameProfile(ByVal bNew As Boolean)
        Dim sProfileName As String = Nothing
        Dim iColumn As Integer = 0

        If bNew = True Then
            Dim profiles As StringCollection = New StringCollection
            For iCount As Integer = 0 To Dgv.ColumnCount - 1
                profiles.Add(Dgv.Columns(iCount).HeaderText)
                NewProfileDialog.combobCopySource.Items.Add(Dgv.Columns(iCount).HeaderText)
            Next
            Dim dResult = NewProfileDialog.ShowDialog()
            If dResult = DialogResult.OK Then
                ' clone column (doesn´t clone values)
                sProfileName = NewProfileDialog.txtProfileName.Text
                If NewProfileDialog.checkbCopyOf.Checked = True Then
                    Dim sSourceProfileName = NewProfileDialog.combobCopySource.Text
                    Dim iSourceColumnIndex As Integer = 0
                    Dim iNewColumnIndex As Integer = 0
                    For Each cColumn As DataGridViewColumn In Dgv.Columns
                        If cColumn.HeaderText = sSourceProfileName Then
                            iSourceColumnIndex = cColumn.Index
                            Dgv.Columns.Add(cColumn.Clone)
                            iNewColumnIndex = Dgv.Columns.Count - 1
                            Dgv.Columns(iNewColumnIndex).HeaderText = sProfileName
                            Dgv.Columns(iNewColumnIndex).Name = sProfileName
                            Exit For
                        End If
                    Next
                    ' copy values from source profile
                    For Each row As DataGridViewRow In Dgv.Rows
                        row.Cells(iNewColumnIndex).Value = row.Cells(iSourceColumnIndex).Value
                    Next
                Else
                    ' create new empty profile
                    Dim CheckBoxColumn As DataGridViewCheckBoxColumn
                    CheckBoxColumn = New DataGridViewCheckBoxColumn
                    CheckBoxColumn.HeaderText = sProfileName
                    CheckBoxColumn.Name = sProfileName
                    CheckBoxColumn.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells
                    Dgv.Columns.Add(CheckBoxColumn)
                End If
            Else
                Exit Sub
            End If
        Else
            ' rename existing profile
            iColumn = Dgv.SelectedCells(0).ColumnIndex
            sProfileName = InputBox(ResourceManager.GetString("newprofile", ci), "", Dgv.Columns(iColumn).HeaderText)
            If sProfileName <> "" Then
                For Each cColumn As DataGridViewColumn In Dgv.Columns
                    If cColumn.HeaderText.ToLower = sProfileName.ToLower Then
                        MsgBox("This profile already exists, please use another name.", MsgBoxStyle.Information)
                        Exit Sub
                    End If
                Next
            Else
                Exit Sub
            End If
            Try
                Directory.Delete(sProfilesFolder + "\" + Dgv.Columns(iColumn).HeaderText, True)
            Catch ex As Exception
                MsgBox(ex.Message)
            End Try
            Dgv.Columns(iColumn).HeaderText = sProfileName
        End If
    End Sub

    ''' <summary>
    ''' Clear Datagridview and load again
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub RefreshmodsToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles RefreshmodsToolStripMenuItem.Click
        _refreshDatagrid()
    End Sub

    Private Sub _refreshDatagrid(Optional ByRef bAskSave As Boolean = True)
        ' Unsaved changes?
        If btnSave.BackColor = Color.Coral And bAskSave Then
            MsgBox(ResourceManager.GetString("savefirst", ci), MsgBoxStyle.Information)
            Exit Sub
        End If
        ' remove all rows
        For n As Integer = 0 To Dgv.Rows.Count - 1
            If Dgv.Rows(0).IsNewRow = False Then
                Dgv.Rows.RemoveAt(0)
            End If
        Next
        ' remove all columns
        For n As Integer = 0 To Dgv.Columns.Count - 1
            Dgv.Columns.RemoveAt(0)
        Next

        ' load again
        fillDatagrid()
        ' This just causes Dgv to refresh (obsolete)
        SendKeys.Send("{tab}")
    End Sub

    ''' <summary>
    ''' Search for a mod (row will be selected and scrolled to)
    ''' </summary>
    Private Sub _FindMod(ByVal bShowdialog As Boolean)
        If bShowdialog = True Then
            SearchMod.ShowDialog()
            If SearchMod.txtSearch.Text <> "" And SearchMod.txtSearch.Text <> lastSearch.sSearchString Then
                With lastSearch
                    .sSearchString = SearchMod.txtSearch.Text
                    .iOccurenceRow = 0
                End With
            ElseIf SearchMod.txtSearch.Text = "" Then
                Exit Sub
            End If
        End If

        Dim iStartindex As Integer = 0
        If (SearchMod.txtSearch.Text <> "" And bShowdialog = True) Or (lastSearch.sSearchString <> "" And bShowdialog = False) Then
            Dim searchValue As String = ""
            If bShowdialog = True Then
                searchValue = SearchMod.txtSearch.Text
            Else
                searchValue = lastSearch.sSearchString
            End If

            searchValue = searchValue.ToLower
            If searchValue = lastSearch.sSearchString Then
                iStartindex = lastSearch.iOccurenceRow + 1
            End If
            Dim matchRowIndex As Integer = -1
            For iIndex As Integer = iStartindex To Dgv.RowCount - 1
                Dim row As DataGridViewRow = Dgv.Rows(iIndex)
                If row.HeaderCell.Value.ToString.ToLower.Contains(searchValue) Then
                    matchRowIndex = row.Index
                    Dgv.Rows(matchRowIndex).Selected = True
                    Dgv.FirstDisplayedScrollingRowIndex = matchRowIndex
                    With lastSearch
                        .sSearchString = searchValue
                        .iOccurenceRow = matchRowIndex
                    End With
                    Exit For
                End If
            Next
        End If
    End Sub

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        saveProfiles()
    End Sub

    ''' <summary>
    ''' Display number of active mods in active profile
    ''' </summary>
    Private Sub _CountActiveMods()
        Try
            Dim iProfileIndex As Integer = Dgv.SelectedCells(0).ColumnIndex
            Dim iCount As Integer = 0
            For Each row As DataGridViewRow In Dgv.Rows
                If row.Cells(iProfileIndex).Value = True Then
                    iCount += 1
                End If
            Next
            TSSLActiveMods.Text = ResourceManager.GetString("ActiveMods", ci) + iCount.ToString + "  |"
        Catch ex As Exception
            TSSLActiveMods.Text = ResourceManager.GetString("ActiveMods", ci) + "n/a  |"
        End Try
    End Sub

    ''' <summary>
    ''' Check if game is running
    ''' </summary>
    ''' <returns></returns>
    Private Function _IsFS15Active() As Boolean
        Dim bRet As Boolean = False
        Try
            Dim listProc() As System.Diagnostics.Process
            listProc = System.Diagnostics.Process.GetProcessesByName("FarmingSimulator2015Game")
            If listProc.Length > 0 Then
                MsgBox(ResourceManager.GetString("GameRunning", ci), MsgBoxStyle.Information)
                bRet = True
            End If
        Catch ex As Exception

        End Try
        Return bRet
    End Function

    Private Sub EinstellungenToolStripMenuItem_Click(sender As Object, e As EventArgs) _
        Handles EinstellungenToolStripMenuItem.Click
        Settings.ShowDialog()
    End Sub

    Private Sub BeendenToolStripMenuItem_Click(sender As Object, e As EventArgs) _
        Handles BeendenToolStripMenuItem.Click
        If btnSave.BackColor = Color.Coral Then
            Dim answer As MsgBoxResult = MsgBox(ResourceManager.GetString("savebeforeexit", ci), MsgBoxStyle.YesNo)
            If answer = MsgBoxResult.Yes Then
                saveProfiles()
            End If
        End If
        Me.Dispose()
    End Sub

    Private Sub AboutToolStripMenuItem_Click(sender As Object, e As EventArgs) _
        Handles AboutToolStripMenuItem.Click
        AboutBox1.ShowDialog()
    End Sub

    Private Sub WelcomeToolStripMenuItem_Click(sender As Object, e As EventArgs) _
        Handles WelcomeToolStripMenuItem.Click
        welcome.ShowDialog()
    End Sub

    Private Sub DeactivateAllToolStripMenuItem_Click(sender As Object, e As EventArgs) _
        Handles DeactivateAllToolStripMenuItem.Click
        toggleAll(False)
    End Sub

    Private Sub ActivateAllToolStripMenuItem_Click(sender As Object, e As EventArgs) _
        Handles ActivateAllToolStripMenuItem.Click
        toggleAll(True)
    End Sub

    Private Sub ActivateAllModsForThisProfileToolStripMenuItem_Click(sender As Object, e As EventArgs) _
        Handles ActivateAllModsForThisProfileToolStripMenuItem.Click
        toggleAll(True)
    End Sub

    Private Sub DeactivateAllModsForThisProfileToolStripMenuItem_Click(sender As Object, e As EventArgs) _
        Handles DeactivateAllModsForThisProfileToolStripMenuItem.Click
        toggleAll(False)
    End Sub

    Private Sub OpenmodRepositoryToolStripMenuItem_Click(sender As Object, e As EventArgs) _
        Handles OpenmodRepositoryToolStripMenuItem.Click
        Process.Start("explorer.exe", sModRepoFolder)
    End Sub

    Private Sub OpenSelectedProfileFolderInExplorerToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles OpenSelectedProfileFolderInExplorerToolStripMenuItem.Click
        openProfileInExplorer()
    End Sub

    Private Sub DgvChanged(sender As Object, e As EventArgs) Handles Dgv.CellValueChanged
        btnSave.BackColor = Color.Coral
    End Sub

    Private Sub btnStartFS15_Click(sender As Object, e As EventArgs) Handles btnStartFS15.Click
        If bGameControllerReminder Then
            MsgBox(ResourceManager.GetString("gamecontroller", ci), MsgBoxStyle.Information, ResourceManager.GetString("reminder", ci))
        End If
        Process.Start(sGameExeFile)
    End Sub

    Private Sub ResetGameToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ResetGameToolStripMenuItem.Click
        If MsgBox(ResourceManager.GetString("resetgamesettings", ci), MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then
            FS15ConfigFile.resetConfig(sGameSettingsXML)
            displayActiveProfile()
        End If
    End Sub

    Private Sub NewProfileToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles NewProfileToolStripMenuItem.Click
        newOrRenameProfile(True)
    End Sub

    Private Sub RenameProfileToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles RenameProfileToolStripMenuItem.Click
        newOrRenameProfile(False)
    End Sub

    Private Sub OpenSelectedprofileFolderInExplorerToolStripMenuItem1_Click(sender As Object, e As EventArgs) Handles OpenSelectedprofileFolderInExplorerToolStripMenuItem1.Click
        openProfileInExplorer()
    End Sub

    Private Sub NewRpfileToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles NewRpfileToolStripMenuItem.Click
        newOrRenameProfile(True)
    End Sub

    Private Sub RenameSelectedProfileToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles RenameSelectedProfileToolStripMenuItem.Click
        newOrRenameProfile(False)
    End Sub

    Private Sub DeleteThisProfileToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles DeleteThisProfileToolStripMenuItem.Click
        deleteSelectedProfile()
    End Sub

    Private Sub DeleteSelectedProfileToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles DeleteSelectedProfileToolStripMenuItem.Click
        deleteSelectedProfile()
    End Sub

    Private Sub Form1_FormClosing(sender As Object, e As FormClosingEventArgs) Handles MyBase.FormClosing
        _exitWithSaving()
    End Sub

    Private Sub _exitWithSaving()
        ' Store location and size
        My.Settings.Form1Location = Location
        My.Settings.Form1Size = Size

        ' Pending changes?
        If btnSave.BackColor = Color.Coral Then
            Dim answer As MsgBoxResult = MsgBox(ResourceManager.GetString("savebeforeexit", ci), MsgBoxStyle.YesNo)
            If answer = MsgBoxResult.Yes Then
                saveProfiles()
            End If
        End If
        Me.Dispose()
    End Sub

    Private Sub ModInfoToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ModInfoToolStripMenuItem.Click
        _prepareShowModInfo()
    End Sub

    Private Sub ShowModinformationCrtlIToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ShowModinformationCrtlIToolStripMenuItem.Click
        _prepareShowModInfo()
    End Sub

    Private Sub DeleteSelectedModToolStripMenuItem1_Click(sender As Object, e As EventArgs) Handles DeleteSelectedModToolStripMenuItem1.Click
        DeleteSelectedMod()
    End Sub

    Private Sub DeleteSelectedModToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles DeleteSelectedModToolStripMenuItem.Click
        DeleteSelectedMod()
    End Sub

    Private Sub _prepareShowModInfo()
        Dim iRowIndex As Integer = Dgv.CurrentCell.RowIndex
        Dim sModFileName As String = sModRepoFolder + "\" + Dgv.Rows(iRowIndex).HeaderCell.Value + ".zip"
        Dim minfo As modInfo = New modInfo(sModFileName)
        showModInfo.txtAuthor.Text = minfo.sAuthor
        showModInfo.txtDescription.Text = minfo.sDescription
        showModInfo.txtFilesize.Text = minfo.dFilesize.ToString + " kBytes"
        showModInfo.txtTitle.Text = minfo.sTitle
        showModInfo.txtVersion.Text = minfo.sVersion
        showModInfo.ilImageList = minfo.ilImagelist
        showModInfo.ShowDialog()
        minfo.Dispose()
    End Sub

    Private Sub Form1_KeyDown(sender As Object, e As KeyEventArgs) Handles MyBase.KeyDown
        ' save profile
        If e.Control And e.KeyCode.ToString = "S" Then
            saveProfiles()
        End If
        ' show mod info
        If e.Control And e.KeyCode.ToString = "I" Then
            _prepareShowModInfo()
        End If
        ' find mod 
        If e.Control And e.KeyCode.ToString = "F" Then
            _FindMod(True)
        End If
        ' activate selected profile
        If e.Control And e.KeyCode.ToString = "P" Then
            Dim sProfile As String = FS15ConfigFile.replaceConfig(sGameSettingsXML, sProfilesFolder, Dgv)
            displayActiveProfile()
        End If
        ' start fs15
        If e.KeyCode = Keys.F5 Then
            Process.Start(sGameExeFile)
        End If
        ' exit program
        If e.Control And (e.KeyCode.ToString = "X" Or e.KeyCode.ToString = "Q") Then
            _exitWithSaving()
        End If
        ' new profile
        If e.Control And e.KeyCode.ToString = "N" Then
            newOrRenameProfile(True)
        End If
        ' rename profile
        If e.KeyCode = Keys.F2 Then
            newOrRenameProfile(False)
        End If
        ' Search next occurence
        If e.KeyCode = Keys.F3 Then
            _FindMod(False)
        End If
        ' refresh datagrid
        If e.Control And e.KeyCode.ToString = "R" Then
            _refreshDatagrid()
        End If
    End Sub

    Private Sub ActivethisModInAllProfilesToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ActivethisModInAllProfilesToolStripMenuItem.Click
        _activateModInAllProfiles()
    End Sub

    Private Sub ActivethisModInAllProfilesToolStripMenuItem1_Click(sender As Object, e As EventArgs) Handles ActivethisModInAllProfilesToolStripMenuItem1.Click
        _activateModInAllProfiles()
    End Sub

    Private Sub FindModToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles FindModToolStripMenuItem.Click
        _FindMod(True)
    End Sub

    Private Sub ChangeLanguage(ByVal lang As String)
        For Each c As Control In Me.Controls
            Dim resources As ComponentResourceManager = New ComponentResourceManager(GetType(Form1))
            resources.ApplyResources(c, c.Name, New CultureInfo(lang))
        Next c
    End Sub
End Class

Public Class clLastSearch
    Public sSearchString As String
    Public iOccurenceRow As Integer
End Class
