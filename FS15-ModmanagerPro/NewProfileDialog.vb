﻿Imports System.IO
Imports System.Globalization

Public Class NewProfileDialog
    Dim ci As CultureInfo = CultureInfo.CurrentCulture
    ''' <summary>
    ''' Toggle activation of Copysource Combobox
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub cbCopyOf_CheckedChanged(sender As Object, e As EventArgs) Handles checkbCopyOf.CheckedChanged
        If checkbCopyOf.Checked = True Then
            combobCopySource.Enabled = True
        Else
            combobCopySource.Enabled = False
        End If
    End Sub

    Private Sub NewProfileDialog_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        combobCopySource.Enabled = False
    End Sub

    Private Sub btnOK_Click(sender As Object, e As EventArgs) Handles btnOK.Click
        txtProfileName.Text = _ClearProfileName(txtProfileName.Text)
        If txtProfileName.Text = "" Then
            MsgBox(ResourceManager.GetString("giveProfileName", ci), MsgBoxStyle.Information)
            Exit Sub
        Else
            If checkbCopyOf.Checked = True And combobCopySource.Text = "" Then
                MsgBox(ResourceManager.GetString("copyProfileHelptext", ci), MsgBoxStyle.Information)
                Exit Sub
            End If
            If checkbCopyOf.Checked = True Then
                For Each sProfile As String In combobCopySource.Items.ToString
                    If sProfile.ToLower = txtProfileName.Text.ToLower Then
                        MsgBox(ResourceManager.GetString("profileExists", ci), MsgBoxStyle.Information)
                        Exit Sub
                    End If
                Next
            End If
            DialogResult = DialogResult.OK
            Close()
        End If
    End Sub

    ''' <summary>
    ''' No illegal characters, please
    ''' </summary>
    Private Function _ClearProfileName(ByVal sProfilename As String) As String
        Dim invalid As String = New String(Path.GetInvalidPathChars())
        For Each c As Char In invalid
            sProfilename = sProfilename.Replace(c.ToString(), "")
        Next
        ' some more illegal characters according to https://stackoverflow.com/questions/1976007/what-characters-are-forbidden-in-windows-and-linux-directory-names
        ' Quote: You are opening up one huge can of hurt.
        sProfilename = sProfilename.Replace(CChar("\"), "")
        sProfilename = sProfilename.Replace(CChar("/"), "")
        sProfilename = sProfilename.Replace(CChar("*"), "")
        sProfilename = sProfilename.Replace(CChar("?"), "")
        Return sProfilename
    End Function

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        DialogResult = DialogResult.Cancel
        Close()
    End Sub

    Private Sub checkbSGCopyOf_CheckedChanged(sender As Object, e As EventArgs) Handles checkbSGCopyOf.CheckedChanged
        If checkbSGCopyOf.Checked = True Then
            combobCopySGSource.Enabled = True
        Else
            combobCopySGSource.Text = ""
            combobCopySGSource.Enabled = False
        End If
    End Sub
End Class