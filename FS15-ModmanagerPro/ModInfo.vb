﻿Imports System.IO
Imports Ionic.Zip
Imports System.Xml
Imports ImageMagick

Public Class modInfo
    Implements IDisposable
    Public dFilesize As Double
    Public sTitle As String
    Public sVersion As String
    Public sAuthor As String
    Public sDescription As String
    Public sIconFilename As String
    Public ilImagelist As ImageList
    Public bIsDirectory As Boolean

    Sub New(ByVal sFilename As String)
        MyBase.New()
        ' check if we´ve got a directory
        If Not File.Exists(sFilename) Then
            If Directory.Exists(Path.GetDirectoryName(sFilename) + "\" + Path.GetFileNameWithoutExtension(sFilename)) Then
                bIsDirectory = True
                sFilename = Path.GetDirectoryName(sFilename) + "\" + Path.GetFileNameWithoutExtension(sFilename)
            Else
                bIsDirectory = False
            End If
        End If
        Dim attributes As modattributes
        If Not bIsDirectory Then
            dFilesize = getSize(sFilename)
            attributes = readDescription(unzipModdesc(sFilename), "en")
        Else
            Dim di As DirectoryInfo = New DirectoryInfo(sFilename)
            dFilesize = DirSize(di)
            attributes = readDescription(sFilename + "\moddesc.xml", "en")
        End If
        sTitle = attributes.sTitle
        sAuthor = attributes.sAuthor
        sVersion = attributes.sVersion
        sDescription = attributes.sDescription
        If Not attributes.sIconFileName Is Nothing Then
            sIconFilename = attributes.sIconFileName.Replace(vbCrLf, Nothing)
        Else
            sIconFilename = ""
        End If
        If Not bIsDirectory Then
            ilImagelist = getImagesFromZip(sFilename, sIconFilename)
        Else
            ilImagelist = getImagesFromDirectory(sFilename, sIconFilename)
        End If
    End Sub

    ' Keep track of when the object is disposed.
    Protected disposed As Boolean = False

    ' This method disposes the base object's resources.
    Protected Overridable Sub Dispose(ByVal disposing As Boolean)
        If Not Me.disposed Then
            If disposing Then
                Me.ilImagelist = Nothing
            End If
            ' Insert code to free unmanaged resources.
        End If
        Me.disposed = True
    End Sub

#Region " IDisposable Support "
    ' Do not change or add Overridable to these methods.
    ' Put cleanup code in Dispose(ByVal disposing As Boolean).
    Public Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub
    Protected Overrides Sub Finalize()
        Dispose(False)
        MyBase.Finalize()
    End Sub
#End Region
    ''' <summary>
    ''' Returns file size
    ''' </summary>
    ''' <param name="sFilename"></param>
    ''' <returns></returns>
    Private Function getSize(ByVal sFilename) As Double
        Dim dReturn As Double
        If File.Exists(sFilename) Then
            Dim f As FileInfo = New FileInfo(sFilename)
            dReturn = f.Length
        Else
            dReturn = -1
        End If
        Return Math.Round(dReturn / 1024, 0)
    End Function

    ''' <summary>
    ''' unzips moddesc.xml from mod file, returns path to unzipped file
    ''' </summary>
    ''' <param name="sFilename"></param>
    ''' <returns></returns>
    Private Function unzipModdesc(ByVal sFilename) As String
        Dim sReturn As String = ""
        Dim UnpackDirectory As String = Environment.GetEnvironmentVariable("TEMP")
        Try
            Dim zip As ZipFile = ZipFile.Read(sFilename)

            Dim collections = zip.SelectEntries("name=modDesc.xml")
            Try
                For Each t In collections
                    t.Extract(UnpackDirectory, ExtractExistingFileAction.OverwriteSilently)
                Next
                sReturn = UnpackDirectory + "\" + "modDesc.xml"
            Catch ex As Exception
                Debug.WriteLine(ex.Message)
            End Try
            Return sReturn
        Catch ex As Exception
            ' MsgBox(sFilename + " is not a valid zip file!" + vbCrLf + ex.Message, MsgBoxStyle.Critical)
            Return "zip file error"
        End Try
    End Function

    ''' <summary>
    ''' Returns imagelist of dds images found in mod zip file, converted to bmp
    ''' </summary>
    ''' <param name="sFilename"></param>
    ''' <param name="sIconFileName"></param>
    ''' <returns></returns>
    Private Function getImagesFromZip(ByVal sFilename As String, sIconFileName As String) As ImageList
        ' Dim sReturn As Collection = New Collection
        Dim imageList1 As ImageList = New ImageList
        imageList1.ImageSize = New Size(220, 220)
        imageList1.ColorDepth = ColorDepth.Depth32Bit
        MagickNET.SetTempDirectory(Environment.GetEnvironmentVariable("TEMP"))
        Dim sImageFileName As String = ""
        Dim UnpackDirectory As String = Environment.GetEnvironmentVariable("TEMP")
        Dim zip As ZipFile = ZipFile.Read(sFilename)

        ' Get IconImage from modDesc as first image
        If sIconFileName <> "" Then
            sIconFileName = sIconFileName.Replace(vbCrLf, "")
            Dim iconImageFormZip = zip.SelectEntries(sIconFileName)

            Try
                For Each t In iconImageFormZip
                    t.Extract(UnpackDirectory, ExtractExistingFileAction.OverwriteSilently)
                    sImageFileName = UnpackDirectory + "\" + t.FileName.Replace(CChar("/"), CChar("\"))
                    Using memStream As New MemoryStream()
                        Using mgkimage As New MagickImage(sImageFileName)
                            mgkimage.Format = MagickFormat.Bmp
                            ' Write the image to the memorystream
                            mgkimage.Write(memStream)
                            imageList1.Images.Add(Image.FromStream(memStream))
                            Try
                                File.Delete(sImageFileName)
                            Catch ex As Exception
                                Debug.WriteLine(ex.Message)
                            End Try
                        End Using
                    End Using
                Next
            Catch ex As Exception
                Debug.WriteLine(ex.Message)
            End Try
        Else
            ' No iconfilename, probably a map; get pda_map.dds
            Dim sMapFileName As String = "pda_map.dds"
            Dim pdaImage = zip.SelectEntries(sMapFileName)
            Try
                For Each t In pdaImage
                    t.Extract(UnpackDirectory, ExtractExistingFileAction.OverwriteSilently)
                    sImageFileName = UnpackDirectory + "\" + t.FileName.Replace(CChar("/"), CChar("\"))
                    Using memStream As New MemoryStream()
                        Using mgkimage As New MagickImage(sImageFileName)
                            mgkimage.Format = MagickFormat.Bmp
                            ' Write the image to the memorystream
                            mgkimage.Write(memStream)
                            imageList1.Images.Add(Image.FromStream(memStream))
                            Try
                                File.Delete(sImageFileName)
                            Catch ex As Exception
                                Debug.WriteLine(ex.Message)
                            End Try
                        End Using
                    End Using
                Next
            Catch ex As Exception
                Debug.WriteLine(ex.Message)
            End Try
        End If

        ' Get some other store images (if availabe)
        Dim ImageFilesCollection = zip.SelectEntries("name=store*.dds")
        Try
            For Each t In ImageFilesCollection
                t.Extract(UnpackDirectory, ExtractExistingFileAction.OverwriteSilently)
                sImageFileName = UnpackDirectory + "\" + t.FileName.Replace(CChar("/"), CChar("\"))
                ' we don´t want the icon twice
                If sIconFileName = "" Or Not sImageFileName.Contains(sIconFileName) Then
                    Try
                        Using memStream2 As New MemoryStream()
                            ' Create image from dds file
                            Using mgkimage As New MagickImage(sImageFileName)
                                ' Sets the output format to bmp
                                mgkimage.Format = MagickFormat.Bmp
                                ' Write the image to the memorystream
                                mgkimage.Write(memStream2)
                                imageList1.Images.Add(Image.FromStream(memStream2))
                                Try
                                    File.Delete(sImageFileName)
                                Catch ex As Exception
                                    Debug.WriteLine(ex.Message)
                                End Try
                            End Using
                        End Using
                    Catch ex As Exception
                        Debug.WriteLine(ex.Message)
                    End Try
                End If
            Next
        Catch ex As Exception
            Debug.WriteLine(ex.Message)
        End Try

        ' Still no images? Let´s see if there are unreferenced dds files, we´ll just take the first three (max)
        If imageList1.Images.Count = 0 Then
            Dim ImageFilesCollection2 = zip.SelectEntries("name=*.dds")
            Dim iImageCount As Integer = 0
            If ImageFilesCollection2.Count > 0 Then
                For Each t In ImageFilesCollection2
                    t.Extract(UnpackDirectory, ExtractExistingFileAction.OverwriteSilently)
                    sImageFileName = UnpackDirectory + "\" + t.FileName.Replace(CChar("/"), CChar("\"))
                    Using memStream2 As New MemoryStream()
                        ' Create image from dds file
                        Using mgkimage As New MagickImage(sImageFileName)
                            ' Sets the output format to bmp
                            mgkimage.Format = MagickFormat.Bmp
                            ' Write the image to the memorystream
                            mgkimage.Write(memStream2)
                            imageList1.Images.Add(Image.FromStream(memStream2))
                            File.Delete(sImageFileName)
                            iImageCount += 1
                            If iImageCount = 3 Then
                                Exit For
                            End If
                        End Using
                    End Using
                Next
            End If
        End If
        Return imageList1
    End Function

    ''' <summary>
    ''' Gets images from mod directory
    ''' </summary>
    ''' <param name="sModDirectory"></param>
    ''' <param name="sIconFileName"></param>
    ''' <returns></returns>
    Private Function getImagesFromDirectory(ByVal sModDirectory As String, sIconFileName As String) As ImageList
        Dim imageList1 As ImageList = New ImageList
        imageList1.ImageSize = New Size(220, 220)
        imageList1.ColorDepth = ColorDepth.Depth32Bit
        MagickNET.SetTempDirectory(Environment.GetEnvironmentVariable("TEMP"))
        Dim sImageFileName As String = ""
        Dim iImageCount As Integer = 0

        If File.Exists(sIconFileName) Then
            Using memStream2 As New MemoryStream()
                ' Create image from dds file
                Using mgkimage As New MagickImage(sModDirectory + "\" + sImageFileName)
                    ' Sets the output format to bmp
                    mgkimage.Format = MagickFormat.Bmp
                    ' Write the image to the memorystream
                    mgkimage.Write(memStream2)
                    imageList1.Images.Add(Image.FromStream(memStream2))
                    iImageCount += 1
                End Using
            End Using
        Else
            Dim ImageFilesCollection2() = Directory.GetFiles(sModDirectory, "store*.dds", SearchOption.AllDirectories)
            If ImageFilesCollection2.Length > 0 Then
                For Each thisImage In ImageFilesCollection2
                    Using memStream2 As New MemoryStream()
                        ' Create image from dds file
                        Using mgkimage As New MagickImage(thisImage)
                            ' Sets the output format to bmp
                            mgkimage.Format = MagickFormat.Bmp
                            ' Write the image to the memorystream
                            mgkimage.Write(memStream2)
                            imageList1.Images.Add(Image.FromStream(memStream2))
                            iImageCount += 1
                            If iImageCount = 3 Then
                                Return imageList1
                            End If
                        End Using
                    End Using
                Next
            End If
        End If
        Return imageList1
    End Function
    ''' <summary>
    ''' Read values from moddesc.xml
    ''' </summary>
    ''' <param name="sXMLFile"></param>
    ''' <param name="sCi"></param>
    ''' <returns></returns>
    Private Function readDescription(ByVal sXMLFile As String, ByVal sCi As String) As modattributes
        Dim attr As modattributes = New modattributes
        Dim doc As XmlDocument = New XmlDocument()

        If File.Exists(sXMLFile) Then
            sXMLFile = fixXmlErrors(sXMLFile)
            If sXMLFile <> Nothing Then
                Try
                    doc.Load(sXMLFile)
                    attr.sTitle = doc.SelectSingleNode("modDesc/title/" + sCi).InnerText + vbCrLf
                    attr.sAuthor = doc.SelectSingleNode("modDesc/author").InnerText + vbCrLf
                    attr.sVersion = doc.SelectSingleNode("modDesc/version").InnerText + vbCrLf
                    attr.sDescription = doc.SelectSingleNode("modDesc/description/" + sCi).InnerText + vbCrLf
                    Try
                        attr.sIconFileName = doc.SelectSingleNode("modDesc/iconFilename").InnerText + vbCrLf
                        ' dirty workaroud for a map that declared a non existing png file as store image
                        If attr.sIconFileName.ToLower.Contains(".png") Then
                            attr.sIconFileName = ""
                        End If
                    Catch ex As Exception
                        attr.sIconFileName = ""
                    End Try
                Catch ex As Exception
                    Debug.WriteLine(ex.Message)
                End Try
            End If
        End If
        Return attr
    End Function

    ''' <summary>
    ''' Ampersand sign in value crashes xmlreader, replace with 'and' and other weird stuff
    ''' </summary>
    ''' <param name="sXMLFile"></param>
    Private Function fixXmlErrors(sXMLFile As String) As String
        'Dim sNewXmlFile As String = Path.GetDirectoryName(sXMLFile) + "\" + "FS15-ModmanagerPro.tmp.xml"
        Dim sNewXmlFile As String = Environment.GetEnvironmentVariable("TEMP") + "\" + "FS15-ModmanagerPro.tmp.xml"
        Try
            Dim myStreamReaderL1 As System.IO.StreamReader
            Dim myStream As System.IO.StreamWriter

            Dim myStr As String
            myStreamReaderL1 = System.IO.File.OpenText(sXMLFile)
            myStr = myStreamReaderL1.ReadToEnd()
            myStreamReaderL1.Close()
            ' remove first space (found in in deutzFahr9340)
            myStr = myStr.Trim
            ' replace any Ampersand
            myStr = myStr.Replace(" & ", " and ")
            'Save myStr
            myStream = System.IO.File.CreateText(sNewXmlFile)
            myStream.WriteLine(myStr)
            myStream.Close()
            Return sNewXmlFile
        Catch ex As Exception
            Debug.WriteLine(ex.Message)
        End Try
        Return Nothing
    End Function

    ''' <summary>
    ''' This is partly redundant. I know that.
    ''' </summary>
    ''' <param name="sFileName"></param>
    ''' <returns></returns>
    Public Shared Function checkMod(ByVal sFileName As String) As modType
        Dim modReturn As modType = New modType
        Dim zip As ZipFile
        ' Catch damaged zip files (Issue #44)
        Try
            zip = ZipFile.Read(sFileName)
        Catch ex As Exception
            MsgBox(sFileName + " is not a valid zip file!" + vbCrLf + ex.Message, MsgBoxStyle.Critical)
            With modReturn
                .isMap = False
                .isMod = False
                .isScript = False
            End With
            Return modReturn
        End Try
        ' Dim zip As ZipFile = ZipFile.Read(sFileName)
        Dim UnpackDirectory As String = Environment.GetEnvironmentVariable("TEMP")
        Dim entries = zip.EntryFileNames
        modReturn.isMod = False

        For Each entry In entries
            If entry.ToLower = "moddesc.xml" Then
                modReturn.isMod = True
                Dim collections = zip.SelectEntries("name=modDesc.xml")
                Try
                    For Each t In collections
                        t.Extract(UnpackDirectory, ExtractExistingFileAction.OverwriteSilently)
                    Next
                    Dim sModdescfile As String = UnpackDirectory + "\" + "modDesc.xml"
                    If File.Exists(sModdescfile) Then
                        Dim doc As XmlDocument = New XmlDocument()
                        doc.Load(sModdescfile)
                        ' is map?
                        Try
                            If doc.SelectSingleNode("modDesc/maps").HasChildNodes Then
                                modReturn.isMap = True
                            End If
                        Catch ex2 As Exception

                        End Try
                        ' is Script (no storeItems)
                        Try
                            If Not doc.SelectSingleNode("modDesc/storeItems").HasChildNodes Then
                                modReturn.isScript = True
                            End If
                        Catch ex As Exception
                            modReturn.isScript = True
                        End Try
                    End If
                    File.Delete(sModdescfile)
                Catch ex As Exception
                    Debug.WriteLine(ex.Message)
                End Try
                Exit For
            End If
        Next
        Return modReturn
    End Function

    ''' <summary>
    ''' Gets directory size
    ''' </summary>
    ''' <param name="d"></param>
    ''' <returns></returns>
    Public Function DirSize(d As DirectoryInfo) As Long
        Dim Size As Long = 0
        ' Add file sizes.
        Dim fis As FileInfo() = d.GetFiles()
        For Each fi As FileInfo In fis
            Size += fi.Length
        Next
        ' Add subdirectory sizes.
        Dim dis As DirectoryInfo() = d.GetDirectories()
        For Each di As DirectoryInfo In dis
            Size += DirSize(di)
        Next
        Return (Size)
    End Function

End Class

Public Class modattributes
    Public sTitle As String
    Public sAuthor As String
    Public sVersion As String
    Public sDescription As String
    Public sIconFileName As String
End Class

Public Class modType
    Public isMod As Boolean
    Public isMap As Boolean
    Public isScript As Boolean
End Class
