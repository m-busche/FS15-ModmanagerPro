﻿Imports System.Collections.Specialized
Imports System.IO

Public Class FS15ConfigFile
    ''' <summary>
    ''' Patches gameSettings.xml to use selected profile
    ''' </summary>
    ''' <param name="sGameSettingsXML"></param>
    ''' <param name="sProfilesFolder"></param>
    ''' <param name="dgv"></param>
    ''' <returns></returns>
    Public Shared Function replaceConfig(ByVal sGameSettingsXML As String, ByVal sProfilesFolder As String, ByVal dgv As DataGridView) As String
        Dim sBackupConfigFile As String = Path.GetDirectoryName(sGameSettingsXML) + "\" + Path.GetFileNameWithoutExtension(sGameSettingsXML) + ".bak"
        Dim sNewConfigFile As StringCollection = New StringCollection
        Dim sReturn As String = ""
        Try
            Dim srGamesettings As StreamReader = New StreamReader(sGameSettingsXML)
            Dim sLine As String = ""
            Dim iColumn As Integer = dgv.SelectedCells(0).ColumnIndex
            Dim sProfile As String = dgv.Columns(iColumn).HeaderText

            sReturn = sProfile
            sProfile = sProfilesFolder + "\" + sProfile

            While Not srGamesettings.EndOfStream
                sLine = srGamesettings.ReadLine
                If sLine.Contains("modsDirectoryOverride") Then
                    sLine = String.Format("  <modsDirectoryOverride active=""True"" directory=""{0}"" />", sProfile)
                End If
                sNewConfigFile.Add(sLine)
            End While
            srGamesettings.Close()

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

        Try
            If File.Exists(sBackupConfigFile) Then
                File.Delete(sBackupConfigFile)
            End If
            File.Copy(sGameSettingsXML, sBackupConfigFile)
        Catch ex As Exception

        End Try

        Try
            Dim swGamesettings As StreamWriter = New StreamWriter(sGameSettingsXML)
            For Each sNewLine As String In sNewConfigFile
                swGamesettings.WriteLine(sNewLine)
            Next
            swGamesettings.Close()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical)
        End Try
        Return sReturn
    End Function

    ''' <summary>
    ''' Reads active profile from gameSettings.xml
    ''' </summary>
    ''' <param name="sGameSettingsXML"></param>
    ''' <returns></returns>
    Public Shared Function readActiveConfiguration(ByVal sGameSettingsXML As String) As String()
        Dim sLine As String = ""
        Dim sReturn(1) As String
        If Not File.Exists(sGameSettingsXML) Then
            sReturn(0) = "-1"
            Return sReturn
            Exit Function
        End If
        Using srConfigfile As StreamReader = New StreamReader(sGameSettingsXML)
            While Not srConfigfile.EndOfStream
                sLine = srConfigfile.ReadLine
                If sLine.Contains("modsDirectoryOverride") Then
                    ' This is very dangerous and silly, but it works
                    Try
                        sReturn(0) = sLine.Substring(sLine.LastIndexOf(CChar("\")) + 1, sLine.Length - sLine.LastIndexOf(CChar("\")) - 1)
                        sReturn(0) = sReturn(0).Substring(0, sReturn(0).Length - 4)
                        sReturn(1) = sLine.Split(CChar("="))(2)
                        sReturn(1) = sReturn(1).Replace(CChar(""""), "")
                        sReturn(1) = sReturn(1).Substring(0, sReturn(1).Length - 3)
                    Catch ex As Exception
                        MsgBox(ex.Message, MsgBoxStyle.Critical)
                    End Try
                End If

            End While
        End Using
        Return sReturn
    End Function

    ''' <summary>
    ''' Resets gameSettings.xml to game default settings
    ''' </summary>
    ''' <param name="sGameSettingsXML"></param>
    Public Shared Sub resetConfig(ByVal sGameSettingsXML As String)
        Dim sLine As String = ""
        Dim swTmpfile As StreamWriter = New StreamWriter(Environment.GetEnvironmentVariable("TEMP") + "\gameSettings.tmp", False)
        Dim srConfigfile As StreamReader = New StreamReader(sGameSettingsXML)
        While Not srConfigfile.EndOfStream
            sLine = srConfigfile.ReadLine
            If sLine.Contains("modsDirectoryOverride") Then
                sLine = "  <modsDirectoryOverride active=""False"" directory=""C:\Temp"" />"
            End If
            swTmpfile.WriteLine(sLine)
        End While
        srConfigfile.Close()
        swTmpfile.Close()
        File.Delete(sGameSettingsXML)
        File.Move(Environment.GetEnvironmentVariable("TEMP") + "\gameSettings.tmp", sGameSettingsXML)
    End Sub
End Class
