﻿Imports System.IO

Public Class Settings
    Private Sub OK_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OK_Button.Click
        saveThisSettings()
        Me.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.Close()
    End Sub

    Private Sub saveThisSettings()
        If savesettings(txtConfigFile.Text, txtModRepo.Text, txtGameExecutable.Text, cbGameControl.Checked.ToString) = False Then
            MsgBox("Failed to save the settings!")
        End If
    End Sub

    Private Sub Cancel_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Cancel_Button.Click
        Me.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.Close()
    End Sub

    Private Sub Settings_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        If File.Exists(Form1.sSettingsFile) Then
            Dim sSettings() As String = readsettings()
            txtConfigFile.Text = sSettings(0)
            txtModRepo.Text = sSettings(1)
            txtGameExecutable.Text = sSettings(2)
            Try
                If sSettings(3) = "True" Then
                    cbGameControl.Checked = True
                Else
                    cbGameControl.Checked = False
                End If
            Catch ex As Exception

            End Try
        End If
    End Sub

    Private Sub btnOFDSettings_Click(sender As Object, e As EventArgs) Handles btnOFDSettings.Click
        Dim sPath As String = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\my games\FarmingSimulator2015"
        With OpenFileDialog1
            .CheckFileExists = True
            .CheckPathExists = True
            .DefaultExt = "xml"
            .Filter = "xml files (*.xml)|*.xml"
            .FileName = "gameSettings.xml"
            .InitialDirectory = sPath
        End With
        If OpenFileDialog1.ShowDialog = DialogResult.OK Then
            txtConfigFile.Text = OpenFileDialog1.FileName
        End If
    End Sub

    Private Sub btnFBModrepo_Click(sender As Object, e As EventArgs) Handles btnFBModrepo.Click
        Dim sPath As String = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\my games\FarmingSimulator2015"
        FolderBrowserDialog1.SelectedPath = sPath
        If FolderBrowserDialog1.ShowDialog = DialogResult.OK Then
            txtModRepo.Text = FolderBrowserDialog1.SelectedPath
        End If
    End Sub

    Private Sub btnOFDExecutable_Click(sender As Object, e As EventArgs) Handles btnOFDExecutable.Click
        Dim sPath As String = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFilesX86)
        With OpenFileDialog1
            .CheckFileExists = True
            .CheckPathExists = True
            .DefaultExt = "exe"
            .Filter = "executable files (*.exe)|*.exe"
            .FileName = "FarmingSimulator2015.exe"
            .InitialDirectory = sPath
        End With
        If OpenFileDialog1.ShowDialog = DialogResult.OK Then
            txtGameExecutable.Text = OpenFileDialog1.FileName
        End If
    End Sub
End Class
