﻿Imports System.IO
Imports System.Xml.Linq
Imports Microsoft.Win32
Imports System.Globalization


Public Class savegameviewer
    Dim ci As CultureInfo = CultureInfo.CurrentCulture
    Private Sub FolderCSVG_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        Using rk As RegistryKey = Registry.CurrentUser.OpenSubKey("Software\Microsoft\Windows\CurrentVersion\Explorer\Shell Folders")
            If rk IsNot Nothing Then
                Dim DocsPath As [Object] = rk.GetValue("Personal")
                If DocsPath IsNot Nothing Then
                    My.Settings.DocsPath = DocsPath
                    My.Settings.Save()
                    Settings.txtConfigFile.Text = Convert.ToString(My.Settings.DocsPath) +
                "\my games\FarmingSimulator2015\gameSettings.xml"
                End If
                'MessageBox.Show(DocsPath.ToString)
                'MessageBox.Show("No Documents Folder Configuration Found")
            End If
        End Using
        ' I will look now for the game folder in the user's computer

        Dim folder As New IO.DirectoryInfo(Path.Combine(My.Settings.DocsPath & "\my games\FarmingSimulator2015\"))
        Dim subfolders = folder.GetDirectories("savegame*.*")
       
        With Me.ComboBox2
            .ValueMember = "FullName"
            .DisplayMember = "Name"
            .SelectedIndex = -1
            .Sorted = True
            '      .DataSource = subfolders

        End With
        Dim textoprevio As String = ""
        For Each sgfolder In subfolders


            Dim CareerSavegameFile As String = Path.Combine(My.Settings.DocsPath & "\my games\FarmingSimulator2015\" & sgfolder.ToString() & "\careerSavegame.xml")
           
            If Not File.Exists(CareerSavegameFile) Then
                textoprevio = "ZZZ_No Map - "
                '    MsgBox("You have not careersavegamefile.xml in this savegame, please select other")
                '    Exit Sub

            Else
                Dim objXLDoc As XDocument = XDocument.Load(CareerSavegameFile)
                Dim objQuery = objXLDoc.Root.Attributes("mapId")
                For Each objResult As XAttribute In objQuery
                    textoprevio = objResult.Value.ToString.Split(".")(0) & " -"
                Next
            End If
            ComboBox2.Items.Add(textoprevio & sgfolder.ToString)
            Next

        Dim folderMMP As New IO.DirectoryInfo(Path.Combine(My.Settings.DocsPath & "\my games\FarmingSimulator2015\mods\profiles\"))
        Dim subfoldersMMP = folderMMP.GetDirectories("*.*")

        For Each Profile In subfoldersMMP

            ComboBox3.Items.Add(Profile.ToString)
            Next

    End Sub
    Private Sub ComboBox2_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ComboBox2.SelectedIndexChanged
        Dim selectedIndex As Integer
        selectedIndex = ComboBox2.SelectedIndex
        Dim selectedItem As Object
        selectedItem = ComboBox2.SelectedItem.ToString.Split("-")(0)
        My.Settings.SaveGame = selectedItem.ToString()
        My.Settings.Save()
        MessageBox.Show("Selected Item Text: " & selectedItem.ToString() & Microsoft.VisualBasic.Constants.vbCrLf & "Index: " & selectedIndex.ToString())
    End Sub

    Private Sub ComboBox3_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ComboBox3.SelectedIndexChanged
        Dim selectedIndex As Integer
        selectedIndex = ComboBox3.SelectedIndex
        Dim selectedItem As Object
        selectedItem = ComboBox3.SelectedItem.ToString
        My.Settings.ProfileGame = selectedItem.ToString()
        My.Settings.Save()
        MessageBox.Show("Selected Item Text: " & selectedItem.ToString() & Microsoft.VisualBasic.Constants.vbCrLf & "Index: " & selectedIndex.ToString())
    End Sub
End Class