﻿Public Class SearchMod
    Private Sub SearchMod_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.KeyPreview = True
        If txtSearch.SelectionLength = 0 Then
            ' Select all text in the text box.
            txtSearch.SelectAll()
        End If
    End Sub

    ''' <summary>
    ''' Escape closes dialog
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub SearchMod_KeyDown(sender As Object, e As KeyEventArgs) Handles MyBase.KeyDown
        If e.KeyCode = Keys.Escape Then
            Close()
        End If
    End Sub

    Private Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click
        Close()
    End Sub
End Class