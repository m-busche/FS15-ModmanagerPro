# FS15-ModmanagerPro

# FS15-ModmanagerPro

Esta no es la primera herramienta para gestionar los archivos de mods de Farming Simulator.
Y probablemente no será la última. Pero tal vez esta herramienta sea un poco más inteligente que las demás.


![FS-15 ModManagerPro Screenshot](screenshots/Modmanager_es01.JPG)

## ¿Para qué sirve esta herramienta?

FS15-ModmanagerPro le permite organizar en perfiles (mapas) sus archivos de mods. En cada
Perfil, puedes activar / desactivar cada mod por separado. Hay otras herramientas
que hacen lo mismo, pero simplemente, nosotros lo hacemos de una forma más eficaz.

## ¿Por qué utilizar FS15-ModmanagerPro?

Tus archivos de mods se guardarán sólo una vez en tu disco duro - con independencia del número de perfiles que hayas creado. 
Otras herramientas de gestión de mods, requieren copiar cada mod en el directorio asignado a cada perfil. 
Nuestro programa FS15-ModmanagerPro utiliza una moderna característica que poseen los sistemas NTFS llamados enlaces simbólicos. Cada archivo (mod) se almacenará una única
vez en tu disco duro y lo único que creará, será un vínculo a los directorios de perfil. Por lo tanto, podrás ahorrar una gran cantidad de espacio en disco.
Con este sistema, podrás tener un mejor control sobre la selección de mods a utilizar cuando empezamos una nueva partida en un nuevo mapa. 

  * No hay límite en el número de perfiles o de mods.
  * Sólo tienes que manjar una sola carpeta de mods.
  * FS15-ModmanagerPro es ultra rápido. ¡Se pueden crear cientos de enlaces simbólicos en unos pocos milisegundos!
  * Si ha descargado una actualización de un mod, basta con sustituir el archivo en su carpeta principal de mods. 
  * Tendrá una visión clara sobre sus mods y perfiles.
  * La selección de mods para un mapa determinado se realizará por enlaces simbólicos y sólo tendrá que hacerlo una vez
  * Podrás obtener información detallada de cualquier mod incluyendo sus imágenes.
  * Distintos colores para identificar los tipos de mods (script/rojo) (mapa/verde) (resto/blanco)
 
 ![FS-15 ModManagerPro Screenshot](screenshots/Modmanager_es02.JPG) 
  
  * FS15-ModmanagerPro es Software OpenSource, por lo que podrás distribuir o modificar este software libremente.
  * Capacidad multilingüe, FS15-ModmanagerPro está actualamente en Inglés, español y alemán 


## Requisitos

Por supuesto, se requiere una versión del juego de Giants Farming Simulator 2015. Además,
se necesita Microsoft .NET Framework 4.5.1 y tener el disco duro en formato NTFS.

## Instalación

Esta descarga incluye un archivo 'setup.exe', que se instala de la forma habitual.
El programa te ayudará en los primeros pasos, para la creación de tu propio almacén de mods
y la creación de los perfiles.

## Problemas conocidos

El instalador tiene problemas ocasionales para detectar una instalación existente de vcredis2015. En este caso, la instalación de los vcredis puede ser cancelada.

## Importante

FS15-ModmanagerPro necesita derechos como administrador para poder crear los enlaces simbólicos en su sistema de archivos. Esto es una restricción de Microsoft Windows.

## Soporte

Código fuente y soporte se puede encontrar en [Gitlab] (https://gitlab.com/m-busche/FS15-ModmanagerPro).
